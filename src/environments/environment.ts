// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.


export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyAZTUhuokJruuR8d-MnlBqAoVZn4CJ3Y_Y",
  authDomain: "test1-b2971.firebaseapp.com",
  databaseURL: "https://test1-b2971.firebaseio.com",
  projectId: "test1-b2971",
  storageBucket: "test1-b2971.appspot.com",
  messagingSenderId: "624900774076",
  appId: "1:624900774076:web:9f356a499d84c03d8798b9",
  measurementId: "G-ZSRE3H4711"
  }
  };
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
