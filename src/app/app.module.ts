import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';

// add
import {MatExpansionModule} from '@angular/material/expansion'; 
import {MatCardModule} from '@angular/material/card';
import { FormsModule }   from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material';
import {MatInputModule} from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';

import { ClassifyComponent } from './classify/classify.component';
import { ClassifiedComponent } from './classified/classified.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component'; 


import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireAuth } from '@angular/fire/auth';
import {AngularFireStorageModule} from '@angular/fire/storage'; 
import { environment } from '../environments/environment';
import { AngularFirestoreModule, AngularFirestore } from '@angular/fire/firestore';
import { LoginsucComponent } from './loginsuc/loginsuc.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { ListComponent } from './list/list.component';
import { EditbookComponent } from './editbook/editbook.component';


const appRoutes: Routes = [

  { path: 'welcome', component: WelcomeComponent},
  { path: 'classified', component: ClassifiedComponent},
  { path: 'editbook/:id', component: EditbookComponent},
  { path: 'classify', component: ClassifyComponent},
  { path: 'signup', component: SignupComponent},
  { path: 'login', component: LoginComponent},
  { path: 'loginsuc', component: LoginsucComponent},
  { path: 'list', component: ListComponent},
  { path: '',
    redirectTo: '/welcome',
    pathMatch: 'full'
  },
];
@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    ClassifyComponent,
    ClassifiedComponent,
    SignupComponent,
    LoginComponent,
    LoginsucComponent,
    WelcomeComponent,
    ListComponent,
    EditbookComponent,
    
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatExpansionModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    FormsModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    BrowserModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireStorageModule,
    HttpClientModule,
    


    RouterModule.forRoot(appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),
  ],
  providers: [AngularFireAuth,AngularFirestore ],
  bootstrap: [AppComponent]
})
export class AppModule { }